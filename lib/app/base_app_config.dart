
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_sdk/data/api/repositories/user_repository.dart';
import 'package:flutter_chat_sdk/data/api/services/user_service.dart';
import 'package:get/get.dart';

enum Environment { dev, prod, staging }

setupLocator() async {
  //Setup service
  Get.lazyPut<UserService>(() => UserService(), fenix: true);
  // Get.lazyPut<CommonService>(() => CommonService(), fenix: true);
  // // Get.lazyPut<HotelService>(() => HotelService(), fenix: true);
  // // Get.lazyPut<StaticService>(() => StaticService(), fenix: true);

  // //Setup responsitory
  Get.lazyPut<UserRepository>(() => UserRepository(), fenix: true);
  // Get.lazyPut<CommonRepository>(() => CommonRepository(), fenix: true);
  // // Get.lazyPut<HotelRepository>(() => HotelRepository(), fenix: true);
  // // Get.lazyPut<StaticRepository>(() => StaticRepository(), fenix: true);

  // Get.lazyPut<PushNotificationManager>(() => PushNotificationManager(),
  //     fenix: true);
  // // Get.lazyPut<GoogleMapUtil>(() => GoogleMapUtil(), fenix: true);

  // Get.put(AssetPathProvider());
  // Get.put(PhotoProvider());
  // Get.put(PermissionHandleManager());
}

setupStatusBar() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.dark,
      systemNavigationBarIconBrightness: Brightness.dark));
}
