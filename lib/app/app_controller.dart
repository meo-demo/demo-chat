// ignore_for_file: avoid_print

import 'dart:developer';
import 'dart:io';
import 'dart:math';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:event_bus/event_bus.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_sdk/app/app_pages.dart';
import 'package:flutter_chat_sdk/data/api/api_constants.dart';
import 'package:flutter_chat_sdk/data/api/models/responses/user.dart';
import 'package:flutter_chat_sdk/data/api/rest_client.dart';
import 'package:flutter_chat_sdk/data/storage/app_storage.dart';
import 'package:flutter_chat_sdk/data/storage/db/db_manager.dart';
import 'package:flutter_chat_sdk/res/language/localization_service.dart';
import 'package:flutter_chat_sdk/res/theme/app_theme.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/utils/log_data.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'base_app_config.dart';
import 'push_notification_manager.dart';

class AppController extends GetxController {
  Environment? env;
  Rx<Locale?>? locale;
  Rx<ThemeData?>? themeData;
  late PackageInfo packageInfo;
  late String deviceId;
  Rx<AuthState> authState = AuthState.unauthorized.obs;
  User? user;

  init(Environment environment) async {
    env = environment;
    // await Future.wait([initFirebase(), initStorage()]);
    await Future.wait([initStorage()]);
    await setupLocator();
    await initTheme();
    await initLanguage();
    await initAuth();
    // initPhotos();
  }

  Future<void> initAuth() async {
    final storage = Get.find<AppStorage>();
    user = await storage.getUserInfo();
    final token = await storage.getUserAccessToken();
    if (user != null && token != null) {
      await initApi(token: token);

      if (user!.isUpdateProfile == 1) {
        authState.value = AuthState.authorized;
      } else {
        authState.value = AuthState.uncompleted;
      }
    } else {
      await initApi();
      authState.value = AuthState.unauthorized;
    }
  }

  Future<void> initFirebase() async {
    await Firebase.initializeApp();
  }

  Future<void> initStorage() async {
    await Get.put(AppStorage()).init();
    await Get.put(DbManager()).init();
  }

  // Future<void> initPhotos() async {
  //   Get.put(PhotoProvider()).init();
  // }

  logout() async {
    user = null;
    locale = null;
    themeData = null;
    await Get.find<AppStorage>().logout();
    await Get.find<DbManager>().clearAll();
    Get.offAllNamed(AppRoutes.INITIAL);
  }

  Future<void> initTheme() async {
    await Get.put(ThemeService()).init();
    final themeService = Get.find<ThemeService>();
    themeData = themeService.themeData.obs;
    //Lang nghe su thay doi theme khi luu vao storage
    themeService.store.box.listenKey(AppStorage.APP_THEME, (value) {
      if (value == null) return;
      if (themeData != null) {
        themeData!.value = appThemeData[themeService.getAppTheme(value)];
      } else {
        themeData = appThemeData[themeService.getAppTheme(value)].obs;
      }
    });
  }

  Future<void> initLanguage() async {
    await Get.put(LocalizationService()).init();
    final localeService = Get.find<LocalizationService>();
    locale = localeService.getLocale.obs;
    //Lang nghe su thay doi theme khi luu vao storage
    localeService.store.box.listenKey(AppStorage.APP_LANGUAGE, (value) {
      if (value != null) locale!.value = Locale(value);
    });
  }

  initApi({String? token}) async {
    String baseUrl;
    // init api
    switch (env!) {
      case Environment.dev:
        baseUrl = BASE_URL_DEV;
        UPLOAD_PHOTO_URL = UPLOAD_PHOTO_URL_DEV;
        break;
      case Environment.staging:
        baseUrl = BASE_URL_STA;
        UPLOAD_PHOTO_URL = UPLOAD_PHOTO_URL_STA;
        break;
      case Environment.prod:
        baseUrl = BASE_URL_PROD;
        UPLOAD_PHOTO_URL = UPLOAD_PHOTO_URL_PROD;
        break;
    }
    packageInfo = await PackageInfo.fromPlatform();
    RestClient.instance.init(baseUrl,
        accessToken: token ?? "",
        platform: Platform.isAndroid ? "android" : "ios",
        appVersion: packageInfo.version,
        deviceId: await getDeviceId(),
        language: "language");

    Get.put(PushNotificationManager()).init();
  }

  Future<void> updateUserInfo(User userInfo) async {
    if (userInfo.isUpdateProfile == 1) {
      authState.value = AuthState.authorized;
      Get.offAllNamed(AppRoutes.MAIN);
    } else {
      authState.value = AuthState.uncompleted;
      Get.offAllNamed(AppRoutes.SIGNUP);
    }
  }

  Future<String> getDeviceId() async {
    final deviceInfoPlugin = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceId = build.id;
        return build.id; //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceId = data.identifierForVendor ?? "";
        return data.identifierForVendor ?? ""; //UUID for iOS
      }
    } on PlatformException {
      print('Failed to get platform version');
    }
    return "";
  }

  @override
  void onClose() {
    // Close all service
    Get.reset();
    super.onClose();
  }
}

EventBus eventBus = EventBus();

enum AuthState { unauthorized, authorized, uncompleted, new_install }
