import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/res/language/localization_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'app_controller.dart';
import 'app_pages.dart';

class ChatApp extends GetWidget<AppController> {
  const ChatApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Obx(() => GetMaterialApp(
            debugShowCheckedModeBanner: false,
            title: "App Chat",
            translations: LocalizationService(),
            locale: controller.locale?.value,
            theme: controller.themeData?.value,
            initialRoute: _getRoute(),
            getPages: AppPages.pages,
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: const [
              Locale('en', 'US'), // English
              Locale('vi', 'VN'), // German
            ],
            builder: EasyLoading.init(),
          )),
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
    );
  }

  String _getRoute() {
    switch (controller.authState.value) {
      case AuthState.unauthorized:
        return AppRoutes.INITIAL;
      case AuthState.authorized:
        return AppRoutes.MAIN;
      case AuthState.new_install:
        return AppRoutes.MAIN;
      case AuthState.uncompleted:
        return AppRoutes.SIGNUP;
      default:
        return AppRoutes.MAIN;
    }
    // return AppRoutes.INIT_ACCOUNT;
  }
}


// mixin HandleNotificationMixin {
//   handleNotification(NotificationItem notificationItem) async {}
// }
