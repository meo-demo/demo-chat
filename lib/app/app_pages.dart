import 'package:flutter_chat_sdk/ui/auth/login/login_controller.dart';
import 'package:flutter_chat_sdk/ui/auth/login/login_page.dart';
import 'package:flutter_chat_sdk/ui/auth/signup/signup_controller.dart';
import 'package:flutter_chat_sdk/ui/auth/signup/signup_page.dart';
import 'package:flutter_chat_sdk/ui/auth/signup/init_account_controller.dart';
import 'package:flutter_chat_sdk/ui/auth/signup/init_account_page.dart';
import 'package:flutter_chat_sdk/ui/main/main_controller.dart';
import 'package:flutter_chat_sdk/ui/main/main_page.dart';
import 'package:get/get.dart';

part 'app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
        name: AppRoutes.INITIAL,
        page: () => LoginPage(),
        binding: BindingsBuilder(() {
          Get.put(LoginController());
        })),
    GetPage(
        name: AppRoutes.INIT_ACCOUNT,
        page: () => InitAccountPage(),
        binding: BindingsBuilder(() {
          Get.put(InitAccountController());
        })),
    GetPage(
        name: AppRoutes.SIGNUP,
        page: () => SignupPage(),
        binding: BindingsBuilder(() {
          Get.put(SignupController());
        })),
    GetPage(
        name: AppRoutes.LOGIN,
        page: () => LoginPage(),
        binding: BindingsBuilder(() {
          Get.put(LoginController());
        })),
    GetPage(
        name: AppRoutes.MAIN,
        page: () => MainPage(),
        binding: BindingsBuilder(() {
          Get.put(MainController());
        }))
  ];
}
