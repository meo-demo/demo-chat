// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';

abstract class AppRoutes {
  static const INITIAL = '/';
  static const INIT_ACCOUNT = '/update_profile';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const OTP = '/otp';
  static const INTRODUCE = '/introduce';
  static const MAIN = '/main';
}
