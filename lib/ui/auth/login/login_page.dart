import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_chat_sdk/res/dimens.dart';
import 'package:flutter_chat_sdk/res/images.dart';
import 'package:flutter_chat_sdk/res/style.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/ui/auth/login/login_controller.dart';
import 'package:flutter_chat_sdk/ui/base/base_page.dart';
import 'package:flutter_chat_sdk/ui/widget/button/custom_button.dart';
import 'package:flutter_chat_sdk/ui/widget/image_widget.dart';
import 'package:flutter_chat_sdk/ui/widget/loading_widget.dart';
import 'package:flutter_chat_sdk/ui/widget/text_field_widget.dart';
import 'package:get/get.dart';

class LoginPage extends BasePage<LoginController> {
  LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: super.build(context),
    );
  }

  @override
  Widget buildContentView(BuildContext context, LoginController controller) {
    return SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 40.ws),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                DImages.appLog,
                height: 100.hs,
                width: 100.ws,
              ),
              SizedBox(
                height: 50.hs,
              ),
              SizedBox(
                height: 50.hs,
              ),
              _buildAppTextField(
                controller: controller.userNameTextEditController,
                inputType: TextInputType.text,
                hintText: txtTranslate('userName'),
              ),
              SizedBox(
                height: 50.hs,
              ),
              _buildAppTextField(
                controller: controller.passwordTextEditController,
                obscureText: true,
                inputType: TextInputType.text,
                hintText: txtTranslate('password'),
              ),
              SizedBox(
                height: 25.hs,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "${txtTranslate('accountnotyet')} ",
                    style: text18,
                  ),
                  InkWell(
                    onTap: () => controller.goToSignupPage(),
                    child: Text(
                      txtTranslate('button.singup'),
                      style: text18.textColorPrimary,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 25.hs,
              ),
              Obx(() => CustomButton(
                    width: MediaQuery.of(context).size.width,
                    height: 43.hs,
                    isEnable: controller.isEnable.value,
                    text: txtTranslate('button.signin'),
                    textStyle: controller.isEnable.value
                        ? text16.bold.textColorWhite
                        : text16.bold.hint,
                    radius: 5.rs,
                    onPressed: () {
                      controller.loginUser(
                          controller.userNameTextEditController.text,
                          controller.passwordTextEditController.text);
                    },
                  ))
            ],
          ),
        ));
  }

  Widget _buildAppTextField(
      {TextEditingController? controller,
      required String hintText,
      TextInputType? inputType,
      Widget? prefixIcon,
      Widget? suffixIcon,
      bool? obscureText,
      bool? readOnly}) {
    return SizedBox(
      height: 33.hs,
      child: AppTextField(
        readOnly: readOnly ?? false,
        controller: controller,
        hintText: hintText,
        maxLength: 11,
        obscureText: obscureText,
        inputType: inputType ?? TextInputType.number,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
      ),
    );
  }
}
