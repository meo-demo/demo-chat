import 'package:flutter/cupertino.dart';
import 'package:flutter_chat_sdk/app/app_pages.dart';
import 'package:flutter_chat_sdk/data/api/repositories/user_repository.dart';
import 'package:flutter_chat_sdk/ui/base/base_controller.dart';
import 'package:get/get.dart';

class LoginController extends BaseController {
  final _userRepository = Get.find<UserRepository>();
  TextEditingController userNameTextEditController = TextEditingController();
  TextEditingController passwordTextEditController = TextEditingController();
  RxBool isEnable = false.obs;

  @override
  void onInit() {
    super.onInit();
    userNameTextEditController.addListener(() {
      checkEnableButton();
    });
    passwordTextEditController.addListener(() {
      checkEnableButton();
    });
  }

  @override
  void dispose() {
    userNameTextEditController.dispose();
    passwordTextEditController.dispose();
    super.dispose();
  }

  void checkEnableButton() {
    if (userNameTextEditController.text.isEmpty ||
        passwordTextEditController.text.isEmpty) {
      isEnable.value = false;
    } else {
      isEnable.value = true;
    }
  }

  Future<void> loginUser(String userName, String password) async {
    try {
      showLoading();
      await _userRepository.loginUser(userName, password);
      hideLoading();
    } catch (e) {
      hideLoading();
      showErrors(e);
    }
  }

  void goToSignupPage() {
    Get.toNamed(AppRoutes.INIT_ACCOUNT);
  }
}
