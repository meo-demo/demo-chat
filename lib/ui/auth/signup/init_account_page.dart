import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/res/style.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/ui/auth/signup/init_account_controller.dart';
import 'package:flutter_chat_sdk/ui/base/base_page.dart';
import 'package:flutter_chat_sdk/ui/widget/button/custom_button.dart';
import 'package:flutter_chat_sdk/ui/widget/image_widget.dart';
import 'package:flutter_chat_sdk/ui/widget/text_field_widget.dart';
import 'package:get/get.dart';

class InitAccountPage extends BasePage<InitAccountController> {
  final appBarHeight = AppBar().preferredSize.height;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: super.build(context),
    );
  }

  @override
  Widget buildContentView(
      BuildContext context, InitAccountController controller) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(context),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      leading: IconButton(
          icon: ImageLoader.svg(DIcons.iconBack,
              height: 32.hs, width: 32.ws, color: getColor().themeColorPrimary),
          onPressed: (() => Get.back())),
      backgroundColor: getColor().bgThemeColorWhite,
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 40.ws),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 19.hs,
            ),
            Text(
              txtTranslate("personal.info"),
              style: text28.textColorBlack,
            ),
            SizedBox(
              height: 10.hs,
            ),
            Text(
              txtTranslate("info.requirement"),
              style: text16.textColorBlack,
            ),
            SizedBox(
              height: 60.hs,
            ),
            _buildInputItem(
                txtTranslate("info.fullname"),
                txtTranslate("info.fullname"),
                controller.fullNameTextEditingController),
            _buildInputItem(
                txtTranslate("info.birthday"),
                txtTranslate("hint.birthday"),
                controller.birhtdayTextEditingController,
                suffixIconUrl: DIcons.iconCalendar,
                readOnly: true,
                onPressSuffixIcon: () => controller.showMyDatePicker(context)),
            _buildInputItem(
                txtTranslate("info.email"),
                txtTranslate("info.email"),
                controller.emailTextEditingController,
                inputType: TextInputType.emailAddress),
            _buildInputItem(
                txtTranslate("info.phone"),
                txtTranslate("info.phone"),
                controller.phoneTextEditingController,
                inputType: TextInputType.number),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(txtTranslate('return.loginpage'), style: text18),
                InkWell(
                  onTap: () => controller.goToLoginPage(),
                  child: Text(txtTranslate('button.signin'),
                      style: text18.textColorPrimary),
                )
              ],
            ),
            SizedBox(
              height: 20.hs,
            ),
            Obx(() => _buildButtonContinute(context))
          ],
        ),
      ),
    );
  }

  Widget _buildInputItem(
    String title,
    String hintText,
    TextEditingController controller, {
    String? suffixIconUrl,
    bool? readOnly,
    Function()? onPressSuffixIcon,
    TextInputType? inputType,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: text14.textColorBlack.bold,
        ),
        SizedBox(
          height: 14.hs,
        ),
        AppTextField(
          inputType: inputType,
          readOnly: readOnly,
          controller: controller,
          hintText: hintText,
          suffixIcon: suffixIconUrl != null
              ? InkWell(
                  onTap: onPressSuffixIcon,
                  child: ImageLoader.svg(suffixIconUrl,
                      color: getColor().suffixIconb7b7b7),
                )
              : const SizedBox(),
        ),
        SizedBox(
          height: 30.hs,
        )
      ],
    );
  }

  Widget _buildButtonContinute(BuildContext context) {
    return CustomButton(
      isEnable: controller.isEnable.value,
      height: 50.hs,
      width: MediaQuery.of(context).size.width,
      text: txtTranslate("button.continute"),
      onPressed: () => controller.continuteSignup(),
      radius: 5.rs,
      textStyle: controller.isEnable.value
          ? text18.bold.textColorWhite
          : text18.bold.hint,
      background: getColor().themeColorPrimary,
    );
  }
}
