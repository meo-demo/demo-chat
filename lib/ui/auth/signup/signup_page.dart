import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_chat_sdk/res/dimens.dart';
import 'package:flutter_chat_sdk/res/icons.dart';
import 'package:flutter_chat_sdk/res/theme/text_theme.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/ui/auth/signup/signup_controller.dart';
import 'package:flutter_chat_sdk/ui/base/base_page.dart';
import 'package:flutter_chat_sdk/ui/widget/button/custom_button.dart';
import 'package:flutter_chat_sdk/ui/widget/image_widget.dart';
import 'package:flutter_chat_sdk/ui/widget/text_field_widget.dart';
import 'package:get/get.dart';

class SignupPage extends BasePage<SignupController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: super.build(context),
    );
  }

  @override
  Widget buildContentView(BuildContext context, SignupController controller) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(context),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      leading: IconButton(
          icon: ImageLoader.svg(DIcons.iconBack,
              height: 32.hs, width: 32.ws, color: getColor().themeColorPrimary),
          onPressed: (() => Get.back())),
      backgroundColor: getColor().bgThemeColorWhite,
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.symmetric(horizontal: 40.ws),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 19.hs,
            ),
            Text(
              txtTranslate("personal.info"),
              style: text28.textColorBlack,
            ),
            SizedBox(
              height: 10.hs,
            ),
            Text(
              txtTranslate("info.requirement"),
              style: text16.textColorBlack,
            ),
            SizedBox(
              height: 60.hs,
            ),
            _buildInputItem(txtTranslate("userName"), txtTranslate("userName"),
                controller.userNameTextEditingController),
            _buildInputItem(txtTranslate("password"), txtTranslate("password"),
                controller.passwordTextEditingController,
                obscureText: true),
            _buildInputItem(
                txtTranslate("confirm.password"),
                txtTranslate("confirm.password"),
                controller.confirmPasswordTextEditingController,
                obscureText: true),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(txtTranslate('return.loginpage'), style: text18),
                InkWell(
                  onTap: () => controller.goToLoginPage(),
                  child: Text(txtTranslate('button.signin'),
                      style: text18.textColorPrimary),
                )
              ],
            ),
            SizedBox(
              height: 161.hs,
            ),
            Obx(() => _buildButtonSignup(context))
          ],
        ),
      ),
    );
  }

  Widget _buildInputItem(
      String title, String hintText, TextEditingController controller,
      {String? suffixIconUrl, bool? obscureText, bool? readOnly}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: text14.textColorBlack.bold,
        ),
        SizedBox(
          height: 14.hs,
        ),
        AppTextField(
          obscureText: obscureText,
          readOnly: readOnly,
          controller: controller,
          hintText: hintText,
          suffixIcon: suffixIconUrl != null
              ? ImageLoader.svg(suffixIconUrl,
                  color: getColor().suffixIconb7b7b7)
              : const SizedBox(),
        ),
        SizedBox(
          height: 30.hs,
        )
      ],
    );
  }

  Widget _buildButtonSignup(BuildContext context) {
    return CustomButton(
      isEnable: controller.isEnable.value,
      height: 50.hs,
      width: MediaQuery.of(context).size.width,
      text: txtTranslate("button.singup"),
      onPressed: () => controller.actionSignup(),
      radius: 5.rs,
      textStyle: controller.isEnable.value
          ? text18.bold.textColorWhite
          : text18.bold.hint,
      background: getColor().themeColorPrimary,
    );
  }
}
