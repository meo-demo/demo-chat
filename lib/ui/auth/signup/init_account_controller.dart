import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/app/app_controller.dart';
import 'package:flutter_chat_sdk/app/app_pages.dart';
import 'package:flutter_chat_sdk/data/api/models/responses/user.dart';
import 'package:flutter_chat_sdk/data/storage/app_storage.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/ui/base/base_controller.dart';
import 'package:flutter_chat_sdk/utils/date_util.dart';
import 'package:get/get.dart';

class InitAccountController extends BaseController {
  TextEditingController fullNameTextEditingController = TextEditingController();
  TextEditingController birhtdayTextEditingController = TextEditingController();
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController phoneTextEditingController = TextEditingController();
  RxBool isEnable = false.obs;
  final birthDayDate = Rx<DateTime>(DateTime.now());
  User initUser = User();
  final _storage = Get.find<AppStorage>();

  @override
  void onInit() {
    fullNameTextEditingController.addListener(() {
      checkButtonEnable();
    });
    birhtdayTextEditingController.addListener(() {
      checkButtonEnable();
    });
    emailTextEditingController.addListener(() {
      checkButtonEnable();
    });
    phoneTextEditingController.addListener(() {
      checkButtonEnable();
    });
    super.onInit();
  }

  void checkButtonEnable() {
    if (fullNameTextEditingController.text.isEmpty ||
        birhtdayTextEditingController.text.isEmpty ||
        emailTextEditingController.text.isEmpty ||
        phoneTextEditingController.text.isEmpty) {
      isEnable.value = false;
    } else {
      isEnable.value = true;
    }
  }

  void continuteSignup() async {
    showLoading();
    initUser.fullName = fullNameTextEditingController.text;
    initUser.birthday = birthDayDate.value;
    initUser.email = emailTextEditingController.text;
    initUser.isUpdateProfile = 0;
    initUser.phone = phoneTextEditingController.text;
    await _storage.saveUserInfo(initUser);
    await _storage.saveUserAccessToken(initUser.token ?? "");
    Get.find<AppController>().updateUserInfo(initUser);
    hideLoading();
  }

  void goToLoginPage() {
    Get.toNamed(AppRoutes.LOGIN);
  }

  void showMyDatePicker(BuildContext context) {
    showDatePicker(
      context: context,
      firstDate: DateTime(1990),
      initialDate: DateTime.now(),
      lastDate: DateTime(2025),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: getColor().themeColorPrimary,
            colorScheme:
                ColorScheme.light(primary: getColor().themeColorPrimary),
          ),
          child: child!,
        );
      },
    ).then((value) => {
          birthDayDate.value = value!,
          birhtdayTextEditingController.text =
              formatDateBirthday(birthDayDate.value)
        });
  }
}
