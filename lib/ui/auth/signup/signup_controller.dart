import 'package:flutter/cupertino.dart';
import 'package:flutter_chat_sdk/app/app_pages.dart';
import 'package:flutter_chat_sdk/data/api/exceptions/my_exception.dart';
import 'package:flutter_chat_sdk/ui/base/base_controller.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class SignupController extends BaseController {
  RxBool isEnable = false.obs;
  TextEditingController userNameTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  TextEditingController confirmPasswordTextEditingController =
      TextEditingController();

  @override
  void onInit() {
    userNameTextEditingController.addListener(() {
      checkButtonEnable();
    });
    passwordTextEditingController.addListener(() {
      checkButtonEnable();
    });
    confirmPasswordTextEditingController.addListener(() {
      checkButtonEnable();
    });
    super.onInit();
  }

  void checkButtonEnable() {
    if (userNameTextEditingController.text.isEmpty ||
        passwordTextEditingController.text.isEmpty ||
        confirmPasswordTextEditingController.text.isEmpty) {
      isEnable.value = false;
    } else {
      isEnable.value = true;
    }
  }

  void actionSignup() {
    if (passwordTextEditingController.text !=
        confirmPasswordTextEditingController.text) {
      MyException myException = MyException(
          title: "Lỗi nhập mật khẩu",
          messageError: "Mật khẩu không trùng khớp");
      showErrors(myException);
    }
  }

  void goToLoginPage() {
    Get.toNamed(AppRoutes.LOGIN);
  }
}
