import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/data/api/exceptions/api_exception.dart';
import 'package:flutter_chat_sdk/data/api/exceptions/my_exception.dart';
import 'package:flutter_chat_sdk/res/dimens.dart';
import 'package:flutter_chat_sdk/res/theme/text_theme.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/ui/widget/button/custom_button.dart';
import 'package:get/get.dart';

import 'base_page.dart';

abstract class BaseController<C> extends GetxController {
  /* Hiển thị các loại thông báo
    1. Hiển thị thông báo full màn hình
    2. Hiển thị thông báo dạng popup
    3.Hiển thị thông báo dạng Snackbar
    */
  int get typeViewNoti => 2;

  @override
  void onInit() async {
    super.onInit();
    viewState = initState.obs;
  }

  late Rx<ViewState> viewState;
  String? errorMessage;

  ViewState get initState => ViewState.loaded;

  showErrors(Object exception) {
    if (exception is ApiException) {
      errorMessage = exception.message;
    } else if (exception is DioError) {
      errorMessage = exception.message;
    } else if (exception is MyException) {
      viewState.value = ViewState.loaded;
      openNotiDialog(exception.messageError);
      return;
    }
    switch (typeViewNoti) {
      case 1:
        viewState.value = ViewState.error;
        break;
      case 2:
        viewState.value = ViewState.loaded;
        openNotiDialog(errorMessage ?? txtTranslate('data.error'));
        break;
      case 3:
        viewState.value = ViewState.loaded;
        Get.snackbar(txtTranslate('noti.title'),
            errorMessage ?? txtTranslate('data.error'),
            backgroundColor: getColor().themeColorWhite);

        break;
    }
  }

  doTask(Function task) async {
    try {
      task();
    } catch (e) {
      print(e);
    }
  }

  showLoading() {
    viewState.value = ViewState.loading;
  }

  hideLoading() {
    viewState.value = ViewState.loaded;
  }

  void onReloadData() {}

  void openNotiDialog(String content, {VoidCallback? callback}) {
    Get.dialog(Material(
      color: Colors.transparent,
      child: Center(
        child: Container(
          padding: EdgeInsets.all(20.ws),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.ws)),
            color: getColor().bgThemeColorWhite,
          ),
          width: 280.ws,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                txtTranslate('noti.title'),
                style: text18.bold.textColorBlack,
              ),
              SizedBox(
                height: 20.hs,
              ),
              Text(
                content,
                style: text14.textColorBlack,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20.hs,
              ),
              CustomButton(
                text: txtTranslate('close'),
                textStyle: text14.bold.textColorWhite,
                onPressed: () {
                  Get.back();
                  if (callback != null) callback();
                },
                width: 100.ws,
                height: 30.ws,
                radius: 6.rs,
                isEnable: true,
              )
            ],
          ),
        ),
      ),
    ));
  }
}
