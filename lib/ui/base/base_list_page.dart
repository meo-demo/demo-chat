
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:flutter_chat_sdk/ui/widget/data_empty_widget.dart';
import 'package:flutter_chat_sdk/ui/widget/loading_widget.dart';
import '../../res/style.dart';
import 'base_list_controller.dart';
import 'base_page.dart';

//ignore: must_be_immutable
abstract class BaseListPage<I, C extends BaseListController<I>>
    extends BasePage<C> {
  BaseListPage({super.key});

  EdgeInsets get padding =>
      const EdgeInsets.symmetric(horizontal: 0, vertical: 0);

  double get itemSpacing => 0;

  Color get dividerColor => colorWhite;

  Color get background => getColor().bgThemeColorWhite;

  Widget buildItem(BuildContext context, I item, int index);

  @override
  Widget buildContentView(BuildContext context, C controller) {
    return Container(
        color: background,
        child: (controller.items.isNotEmpty ||
                controller.viewState.value == ViewState.loading)
            ? CustomScrollView(
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                controller: controller.scrollController,
                slivers: [
                  CupertinoSliverRefreshControl(
                    onRefresh:
                        controller.enableRefresh ? controller.onRrefresh : null,
                    builder: (_, __, a1, a2, a3) {
                      return Container(
                        alignment: Alignment.center,
                        child: const LoadingWidget(
                          radius: 12,
                        ),
                      );
                    },
                  ),
                  SliverPadding(
                    padding: padding,
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) {
                          return Column(
                            children: [
                              buildItem(
                                  context, controller.items[index], index),
                              Divider(height: itemSpacing, color: dividerColor)
                            ],
                          );
                        },
                        childCount: controller.items.length,
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: controller.isLoadMore.value
                        ? Container(
                            padding: const EdgeInsets.only(bottom: 10, top: 10),
                            alignment: Alignment.center,
                            child: const LoadingWidget(
                              radius: 12,
                            ),
                          )
                        : const SizedBox(),
                  ),
                ],
              )
            : DataEmptyWidget(
                background: background,
              ));
  }
}
