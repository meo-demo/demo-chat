import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_chat_sdk/data/api/api_constants.dart';
import 'package:get/get.dart';
import 'base_controller.dart';
import 'base_page.dart';

abstract class BaseListController<I> extends BaseController {
  int totalItem = 0;
  RxList<I> items = <I>[].obs;
  late ScrollController scrollController;
  int page = 1;

  int get pageSize => PAGE_SIZE;

  int get itemCount => items.length;

  RxBool isLoadMore = false.obs;

  int get initIndexPage => 0;

  bool get autoLoadData => true;

  bool continueLoadMore = true;

  double get rangeLoadMore => 500;

  bool get enableRefresh => true;

  bool get isLoadCacheData => false; //Load data from cache

  @override
  ViewState get initState => ViewState.loading;

  @override
  void dispose() {
    scrollController.removeListener(_scrollListener);
    super.dispose();
    scrollController.dispose();
  }

  @override
  Future<void> onReloadData() async {
    getData();
  }

  void _scrollListener() {
    if (scrollController.position.extentAfter == rangeLoadMore &&
        continueLoadMore) {
      loadMoreData();
    }
  }

  loadMoreData({dynamic params}) {
    if (isLoadMore.value) return;
    isLoadMore.value = true;
    loadData(params: params);
  }

  Future<void> onRrefresh({dynamic params}) async {
    if (enableRefresh) {
      page = 1;
      loadData(params: params, isClear: true);
    }
  }

  jumpToTop() {
    scrollController.animateTo(
      0.0,
      curve: Curves.easeInOut,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  void onInit() {
    super.onInit();
    scrollController = ScrollController()..addListener(_scrollListener);
    if (isLoadCacheData) {
      loadCacheData();
    } else if (autoLoadData) {
      loadData();
    }
  }

  loadData({dynamic params, bool isClear = false}) async {
    log("*************************** page: $page ***************************\n\n");
    try {
      final data = await getData(isClear: isClear);
      if (isClear) {
        items.clear();
      }
      continueLoadMore =
          data != null && data.length == PAGE_SIZE ? true : false;
      if (data != null && data.isNotEmpty) {
        items.addAll(data);
        page++;
      }
      viewState.value = ViewState.loaded;
    } catch (e) {
      viewState.value = ViewState.error;
    }
    isLoadMore.value = false;
  }

  loadCacheData({dynamic params}) async {
    try {
      final cacheData = await getCacheData(params: params);
      if (cacheData != null && cacheData.isNotEmpty) {
        items.clear();
        items.addAll(cacheData);
        page++;
      } else {
        viewState.value = ViewState.error;
      }
    } catch (e) {
      viewState.value = ViewState.error;
    }
    isLoadMore.value = false;
  }

  Future<List<I>?> getData({bool? isClear = false});

  Future<List<I>?>? getCacheData({dynamic params}) {
    return null;
  }
}
