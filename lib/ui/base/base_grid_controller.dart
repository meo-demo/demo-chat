import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_chat_sdk/data/api/api_constants.dart';
import 'package:get/get.dart';

import 'base_controller.dart';
import 'base_page.dart';

abstract class BaseGridController<I> extends BaseController {
  int totalItem = 0;
  RxList<I> items = RxList<I>();
  late ScrollController scrollController;
  int page = 1;

  int get pageSize => PAGE_SIZE;

  int get itemCount => items.length;

  RxBool isLoadMore = false.obs;

  int get initIndexPage => 0;

  bool get autoLoadData => true;

  double get rangeLoadMore => 500;

  bool get enableRefresh => true;

  bool get isLoadCacheData => false; //Load data from cache

  @override
  ViewState get initState => ViewState.loading;

  @override
  void dispose() {
    scrollController.removeListener(_scrollListener);
    super.dispose();
    scrollController.dispose();
  }

  @override
  Future<void> onReloadData() async {
    getData();
  }

  void _scrollListener() {
    if (scrollController.position.extentAfter < rangeLoadMore) {
      loadMoreData();
    }
  }

  loadMoreData({dynamic params}) {
    if (!isLoadMore.value) return;
    isLoadMore.value = true;
    loadData(params: params);
  }

  Future<void> onRrefresh({dynamic params}) async {
    if (enableRefresh) {
      page = 1;
      loadData(params: params, isClear: true);
    }
  }

  @override
  void onInit() {
    super.onInit();
    scrollController = ScrollController()..addListener(_scrollListener);
    if (isLoadCacheData) {
      loadCacheData();
    } else if (autoLoadData) {
      loadData();
    }
  }

  loadData({dynamic params, bool isClear = false}) async {
    log("*************************** page: $page ***************************\n\n");
    try {
      final data = await getData();
      if (isClear) {
        items.clear();
      }
      if (data?.isNotEmpty == true) {
        items.addAll(data!);
        page++;
      }
      isLoadMore.value = data != null && data.length == PAGE_SIZE;
      viewState.value = ViewState.loaded;
    } catch (e) {
      viewState.value = ViewState.error;
    }
  }

  loadCacheData({dynamic params}) async {
    try {
      final cacheData = await getCacheData(params: params);
      if (cacheData != null && cacheData.isNotEmpty) {
        items.clear();
        items.addAll(cacheData);
        page++;
      } else {
        viewState.value = ViewState.error;
      }
    } catch (e) {
      viewState.value = ViewState.error;
    }
  }

  Future<List<I>?> getData();

  Future<List<I>?>? getCacheData({dynamic params}) {
    return null;
  }
}
