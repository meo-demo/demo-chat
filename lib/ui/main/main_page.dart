import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/ui/base/base_controller.dart';
import 'package:flutter_chat_sdk/ui/base/base_page.dart';
import 'package:flutter_chat_sdk/ui/main/main_controller.dart';

class MainPage extends BasePage<MainController> {
  @override
  Widget buildContentView(BuildContext context, MainController controller) {
    return Container();
  }
}
