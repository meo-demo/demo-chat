import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';
import 'package:shimmer/shimmer.dart';


class ShimmerWidget extends StatelessWidget {
  final Widget child;
  ShimmerWidget({required this.child});
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: getColor().colorShimmerBase,
      highlightColor: getColor().colorShimmerHighlight,
      period: const Duration(milliseconds: 1000),
      child: child,
    );
  }
}
