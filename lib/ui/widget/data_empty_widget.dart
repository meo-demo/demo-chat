import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/res/theme/theme_service.dart';

import '../../res/style.dart';

class DataEmptyWidget extends StatelessWidget {
  final Color? background;

  const DataEmptyWidget({super.key, this.background});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: background ?? getColor().bgThemeColorWhite,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            DImages.icEmpty,
            width: 120,
            height: 120,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            txtTranslate('data.empty'),
            style: text16.textColorBlack,
          )
        ],
      ),
    );
  }
}
