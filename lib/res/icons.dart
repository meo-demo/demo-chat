class DIcons {
  static String get iconBack => "assets/icons/ic_icon_back.svg";
  static String get iconCalendar => "assets/icons/ic_icon_calendar.svg";
  static String get iconDropdown => "assets/icons/ic_icon_dropdown.svg";
}
