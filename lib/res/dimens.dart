import 'dart:math';

import 'package:get/get.dart';

// Default guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 812;

double get scaleWidth => Get.width / guidelineBaseWidth;

double get scaleHeight => Get.height / guidelineBaseHeight;

double get scaleText => min(scaleWidth, scaleHeight);

class Dimens {
  static const iconSize = 32.0;
  static const spacing = 16.0;
  static const spacing40 = 40.0;
  static const verticalTextSpacing = 8.0;
}

extension ScreenExtension on num {
  double get ws => this * scaleWidth; // Width Scale
  double get hs => this * scaleHeight; // Height Scale
  double get ts => this * scaleText; // Text Scale
  double get rs => this * scaleText; // Radius Scale
}
