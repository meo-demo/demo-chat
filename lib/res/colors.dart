import 'package:flutter/material.dart';

const colorPrimary = Color(0xFFe7bb70);
const colorPrimaryDark = Color(0xFFe7bb70);
const colorWhite = Color(0xFFFFFFFF);
const colorBlack = Color(0xFF000000);
const grayF1Color = Color(0xFFedf0f1);
const gray2eaColor = Color(0xFFe2e2ea);
const colorE2E4E7 = Color(0xFFE2E4E7);
const color787878 = Color(0xFF787878);
const neutralLight = Color.fromRGBO(176, 180, 181, 1);
const lighter = Color.fromRGBO(240, 244, 245, 1);
const b7b7b7 = Color(0xFFb7b7b7);
