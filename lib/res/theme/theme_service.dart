import 'package:flutter/material.dart';
import 'package:flutter_chat_sdk/data/storage/app_storage.dart';
import 'package:flutter_chat_sdk/res/colors.dart';
import 'package:flutter_chat_sdk/res/theme/app_theme.dart';
import 'package:get/get.dart';

import '../colors.dart';

class ThemeService {
  static const LIGHT_THEME = 0;
  static const DARK_THEME = 1;
  final store = Get.find<AppStorage>();
  ThemeData? _themeData;

  ThemeData? get themeData {
    _themeData ??= appThemeData[AppTheme.DARK];
    return _themeData;
  }

  init() async {
    loadTheme();
  }

  Future<void> loadTheme() async {
    final theme = await store.getTheme();
    currentAppTheme = getAppTheme(theme);
    _themeData = appThemeData[currentAppTheme];
  }

  updateTheme(int theme) async {
    currentAppTheme = getAppTheme(theme);
    _themeData = appThemeData[theme];
    store.setTheme(theme);
  }

  AppTheme getAppTheme(int theme) {
    switch (theme) {
      case LIGHT_THEME:
        return AppTheme.LIGHT;
      case DARK_THEME:
        return AppTheme.DARK;
      default:
        return AppTheme.LIGHT;
    }
  }
}

AppTheme currentAppTheme = AppTheme.LIGHT;

ColorScheme getColor() => Get.find<ThemeService>().themeData!.colorScheme;

extension MyColorScheme on ColorScheme {
  Color getColorTheme(Color colorThemeWhite, Color colorThemeDark) {
    switch (currentAppTheme) {
      case AppTheme.LIGHT:
        return colorThemeWhite;
      case AppTheme.DARK:
        return colorThemeDark;
      default:
        return colorThemeWhite;
    }
  }

  //***************************START TEXT COLOR*****************************************
  //Màu của chữ trong toàn bộ ứng dụng

  Color get textColorPrimary => getColorTheme(colorPrimary, colorPrimary);

  Color get textColorWhite => getColorTheme(colorWhite, colorWhite);

  Color get textColorBlack => getColorTheme(colorBlack, colorBlack);

  Color get hintText => getColorTheme(neutralLight, neutralLight);

  //***************************COMPONENT COLOR*****************************************
  //Màu của các button,checkbox.....

  Color get themeColorPrimary => getColorTheme(colorPrimary, colorPrimary);

  Color get themeColorWhite => getColorTheme(colorWhite, colorWhite);

  Color get themeColor787878 => getColorTheme(color787878, color787878);
  Color get buttonDisable => getColorTheme(lighter, lighter);

  Color get themeColorTransparent =>
      getColorTheme(Colors.transparent, Colors.transparent);
  Color get suffixIconb7b7b7 => getColorTheme(b7b7b7, b7b7b7);

//***************************BACKGROUND COLOR*****************************************
  //Màu của các background

  Color get bgThemeColorWhite => getColorTheme(colorWhite, colorWhite);

  Color get bgThemeColorBlack => getColorTheme(colorBlack, colorBlack);
  Color get borderNeutral => getColorTheme(neutralLight, neutralLight);
  Color get colorShimmerBase => getColorTheme(grayF1Color, grayF1Color);
  Color get colorShimmerHighlight => getColorTheme(gray2eaColor, gray2eaColor);
}
