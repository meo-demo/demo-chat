import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_sdk/res/colors.dart';
import 'package:flutter_chat_sdk/res/theme/text_theme.dart';

enum AppTheme { LIGHT, DARK }

final appThemeData = {
  AppTheme.LIGHT: ThemeData(
      primaryColor: colorPrimary,
      primaryColorDark: colorPrimary,
      backgroundColor: colorWhite,
      primaryColorLight: colorPrimary,
      scaffoldBackgroundColor: colorWhite,
      bottomAppBarColor: colorWhite,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      textTheme: createTextTheme(),
      fontFamily: FontFamily,
      brightness: Brightness.light,
      appBarTheme: const AppBarTheme(
        color: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      colorScheme: ColorScheme.fromSwatch().copyWith(secondary: colorPrimary)),
  AppTheme.DARK: ThemeData(
      brightness: Brightness.dark,
      primaryColor: colorPrimary,
      scaffoldBackgroundColor: colorWhite,
      bottomAppBarColor: colorWhite,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      textTheme: createTextTheme(),
      fontFamily: FontFamily,
      appBarTheme: const AppBarTheme(
        color: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      )),
};
