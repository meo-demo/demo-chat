class DImages {
  static String get appLog => "assets/images/img_app_log.png";
  static String get dataError => "assets/images/img_data_error.png";
  static String get icEmpty => "assets/images/img_ic_empty.png";
  static String get icUsFlag => "assets/images/img_ic_us_flag.png";
  static String get icVietnamFlag => "assets/images/img_ic_vietnam_flag.png";
}
