final Map<String, String> enUS = {
  'vi': 'Vietnamese',
  'en': 'English',
  'userName': 'User name',
  'password': 'Password',
  'confirm.password': 'Confirm password',
  'accountnotyet': 'If you not have account yet?',
  'button.singup': 'Signup',
  'button.signin': 'Signin',
  'button.continute': 'Continute',
  'personal.info': 'Personal information',
  'info.requirement':
      'Please complete the personal information below to set up an account',
  'info.fullname': 'Full name',
  'info.birthday': 'Birthday',
  'info.email': 'Email',
  'info.phone': 'Phone number',
  'hint.birthday': 'dd/mm/yyy',
  'noti.title': 'Notification',
  'return.loginpage': 'Return to '
};
