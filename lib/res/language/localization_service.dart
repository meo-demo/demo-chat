// ignore_for_file: non_constant_identifier_names

import 'dart:ui';

import 'package:flutter_chat_sdk/data/storage/app_storage.dart';
import 'package:flutter_chat_sdk/res/images.dart';
import 'package:get/get.dart';

import 'st_en_us.dart';
import 'st_vi_vn.dart';

class LocalizationService extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {"vi": viVN, "en": enUS};

  final store = Get.find<AppStorage>();

  String _language = LANGUAGES[0].key;

  String get currentLanguage => _language;

  init() {
    loadLanguage();
  }

  Future<void> loadLanguage() async {
    _language = await store.getLanguage();
    updateLanguageSelected(_language);
  }

  updateLanguageSelected(String language) {
    LANGUAGES.forEach((element) {
      element.isSelected.value = (element.key == language);
    });
  }

  Locale? get getLocale {
    if (_language.isNotEmpty) return Locale(_language);
    return Get.deviceLocale;
  }

  Future<void> updateLanguage(LanguageItem item) async {
    _language = item.key;
    await store.setLanguage(_language);
    if (getLocale != null) {
      Get.updateLocale(getLocale!);
    }
    updateLanguageSelected(_language);
  }
}

final LANGUAGES = [
  LanguageItem(icon: DImages.icVietnamFlag, name: 'vn.language', key: "vi"),
  LanguageItem(icon: DImages.icUsFlag, name: 'us.language', key: "en")
];

class LanguageItem {
  String icon;
  String name;
  String key;
  RxBool isSelected = false.obs;
  LanguageItem({required this.icon, required this.name, required this.key});
}
