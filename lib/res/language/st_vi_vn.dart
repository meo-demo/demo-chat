final Map<String, String> viVN = {
  'vi': 'Tiếng Việt',
  'en': 'Tiếng Anh',
  'userName': 'Tên đăng nhập',
  'password': 'Mật khẩu',
  'confirm.password': 'Xác nhận mật khẩu',
  'accountnotyet': 'Nếu bạn chưa có tài khoản?',
  'button.singup': 'Đăng ký',
  'button.signin': 'Đăng nhập',
  'button.continute': 'Tiếp tục',
  'personal.info': 'Thông tin cá nhân',
  'info.requirement':
      'Vui lòng hoàn tất thông tin cá nhân dưới đây để thiết lập tài khoản',
  'info.fullname': 'Họ và tên',
  'info.birthday': 'Ngày sinh',
  'info.email': 'Email',
  'info.phone': 'Số điện thoại',
  'hint.birthday': 'dd/mm/yyy',
  'noti.title': 'Thông báo',
  'return.loginpage': 'Quay lại trang '
};
