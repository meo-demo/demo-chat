import 'package:flutter_chat_sdk/res/colors.dart';
import 'package:flutter_chat_sdk/ui/widget/custom_animation.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.custom
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = colorPrimary
    ..backgroundColor = colorE2E4E7
    ..indicatorColor = colorPrimary
    ..textColor = colorPrimary
    ..maskColor = colorPrimary.withOpacity(0.5)
    ..userInteractions = true
    ..dismissOnTap = false
    ..customAnimation = CustomAnimation();
}
