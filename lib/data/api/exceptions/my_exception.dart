class MyException {
  String title;
  String messageError;

  MyException({required this.title, required this.messageError});
}
