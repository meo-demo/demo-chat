import 'package:dio/dio.dart';
import 'package:flutter_chat_sdk/app/app_controller.dart';
import 'package:flutter_chat_sdk/data/api/api_constants.dart';
import 'package:flutter_chat_sdk/utils/log_data.dart';
import 'package:get/get.dart';

class RestClient {
  static const TIMEOUT = 30000;
  static const ENABLE_LOG = true;
  static const ACCESS_TOKEN_HEADER = 'token';
  static const LANGUAGE = 'Accept-Language';

  // singleton
  static final RestClient instance = RestClient._internal();

  factory RestClient() {
    return instance;
  }

  RestClient._internal();

  late String baseUrl;
  late Map<String, dynamic> headers;

  void init(String baseUrl,
      {String? platform,
      String? deviceId,
      String? language,
      String? appVersion,
      String? accessToken}) {
    this.baseUrl = baseUrl;
    headers = {
      'Content-Type': 'application/json',
      'X-Version': appVersion,
      'X-Platform': platform,
      'x-device-id': deviceId
    };
    if (accessToken != null) setToken(accessToken);
    setLanguage(language!);
  }

  void setToken(String token) {
    headers[ACCESS_TOKEN_HEADER] = "Bearer $token";
  }

  void setLanguage(String language) {
    headers[LANGUAGE] = language;
  }

  void clearToken() {
    headers.remove(ACCESS_TOKEN_HEADER);
  }

  static Dio getDio({String? customUrl, bool isUpload = false}) {
    var dio = Dio(
        instance.getDioBaseOption(customUrl: customUrl, isUpload: isUpload));

    if (ENABLE_LOG) {
      dio.interceptors.add(LogInterceptor(
          requestBody: true, responseBody: true, logPrint: logPrint));
    }
    // //check expire time
    dio.interceptors.add(InterceptorsWrapper(
      onError: (DioError error, handler) async {
        handler.next(error);
      },
    ));

    return dio;
  }

  BaseOptions getDioBaseOption({String? customUrl, bool isUpload = false}) {
    return BaseOptions(
      baseUrl: isUpload ? UPLOAD_PHOTO_URL : customUrl ?? instance.baseUrl,
      connectTimeout: TIMEOUT,
      receiveTimeout: TIMEOUT,
      headers: instance.headers,
      responseType: ResponseType.json,
    );
  }
}
