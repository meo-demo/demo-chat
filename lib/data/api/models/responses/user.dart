import 'package:flutter_chat_sdk/utils/date_util.dart';

class User {
  String? userId;
  String? fullName;
  String? userName;
  String? password;
  String? email;
  String? phone;
  DateTime? birthday;
  int? exchange;
  int? isDeleted;
  int? isActive;
  int? isUpdateProfile;
  String? token;

  User(
      {this.userId,
      this.fullName,
      this.userName,
      this.password,
      this.exchange,
      this.isDeleted,
      this.isActive,
      this.isUpdateProfile,
      this.token,
      this.birthday,
      this.phone,
      this.email});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    fullName = json['fullName'];
    email = json['email'];
    phone = json['phone'];
    birthday = convertStringToDateTime(json['birthday']);
    userName = json['userName'];
    password = json['password'];
    exchange = json['exchange'];
    isDeleted = json['isDeleted'];
    isActive = json['isActive'];
    isUpdateProfile = json['isUpdateProfile'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId;
    data['fullName'] = fullName;
    data['userName'] = userName;
    data['phone'] = phone;
    data['email'] = email;
    data['password'] = password;
    data['exchange'] = exchange;
    data['birthday'] = dateToJsonFormatMongoDB(birthday);
    data['isDeleted'] = isDeleted;
    data['isActive'] = isActive;
    data['isUpdateProfile'] = isUpdateProfile;
    data['token'] = token;
    return data;
  }
}
