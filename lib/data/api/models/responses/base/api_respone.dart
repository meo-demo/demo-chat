class ApiResponse {
  ApiResponse({this.data, this.message, this.status, required error});
  int? status;
  String? message;
  String? error;
  dynamic data;

  factory ApiResponse.fromJson(Map<String, dynamic> json) => ApiResponse(
        message: json["message"],
        data: json["data"],
        error: json["error"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() =>
      {"data": data, "message": message, "status": status, "error": error};
}
