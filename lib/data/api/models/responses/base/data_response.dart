class DataResponse {
  dynamic data;
  String message;

  DataResponse(this.data, {this.message = ""});
}
