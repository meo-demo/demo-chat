import 'package:flutter_chat_sdk/app/app_controller.dart';
import 'package:flutter_chat_sdk/data/api/models/requests/login_request.dart';
import 'package:flutter_chat_sdk/data/api/repositories/base_reponsitory.dart';
import 'package:flutter_chat_sdk/data/api/services/user_service.dart';
import 'package:flutter_chat_sdk/data/storage/app_storage.dart';
import 'package:get/get.dart';

class UserRepository extends BaseRepository {
  final UserService _userService = Get.find<UserService>();
  final _storage = Get.find<AppStorage>();
  Future<void> loginUser(String userName, String password) async {
    LoginRequest request = LoginRequest(userName: userName, password: password);
    final userRepo = await _userService.loginUser(request);
    await _storage.saveUserInfo(userRepo);
    await _storage.saveUserAccessToken(userRepo.token ?? "");
    Get.find<AppController>().updateUserInfo(userRepo);
    Get.find<AppController>().initApi(token: userRepo.token);
  }
}
