// ignore_for_file: constant_identifier_names

import 'package:intl/intl.dart';

const DATE_TIME_FORMAT = "yyyy-MM-ddTHH:mm:ssZ";
const DATE_TIME_FORMAT2 = "dd/MM/yyyy HH:mm";
const DATE_TIME_FORMAT3 = 'HH:mm dd/MM/yyyy';
const DATE_TIME_FORMAT4 = 'HH:mm - dd/MM/yyyy';
const DATE_FORMAT = "dd/MM/yyyy";
const DATE_FORMAT2 = "dd-MM-yyyy";
const DATE_OF_WEEK_FORMAT = "EEE, dd/MM";
const TIME_FORMAT = "HH:mm";
final minDate = DateTime(1900, 1, 1, 0, 0, 0);
final maxDate = DateTime(3000, 1, 1, 0, 0, 0);
const PAGE_SIZE = 30;
const BASE_URL_DEV = "https://appchat-servertest.onrender.com/api/v2/";
const UPLOAD_PHOTO_URL_DEV = "";

// DEV
const PHOTO_URL_DEFAULT = '';
const BASE_URL_DEFAULT = '';
const SOCKET = '';

String UPLOAD_PHOTO_URL = PHOTO_URL_DEFAULT;
String BASE_URL = BASE_URL_DEFAULT;
String PHOTO_URL = PHOTO_URL_DEFAULT;
String VIDEO_URL = PHOTO_URL_DEFAULT;
String AUDIO_URL = PHOTO_URL_DEFAULT;

const BASE_URL_PROD = ""; // production
const UPLOAD_PHOTO_URL_PROD = ""; //production
const DOWNLOAD_PHOTO_URL_PROD = ""; // production

const BASE_URL_STA = ""; // staging
const UPLOAD_PHOTO_URL_STA = ""; //staging

const URL_TTGD = "";
const URL_TERM = "";

String PHOTO_URL_CDN = "";

// API
const USER_SIGNUP = "/user/signup";
const USER_SIGNIN = "/user/signin";
const USER_SEND_CONTACT_INVITATION = "/contact/send-invitation";
const USER_CONFIRM_CONTACT_INVITATION = "/contactconfirm-invitation";
const USER_UPLOAD_AVATAR = "/user/upload-avatar";
