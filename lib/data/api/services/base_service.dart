import 'package:flutter_chat_sdk/app/app_controller.dart';
import 'package:flutter_chat_sdk/data/api/exceptions/api_exception.dart';
import 'package:flutter_chat_sdk/data/api/models/responses/base/api_respone.dart';
import 'package:flutter_chat_sdk/data/api/models/responses/base/data_response.dart';
import 'package:flutter_chat_sdk/data/api/rest_client.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;

abstract class BaseService {
  Map<String, dynamic> addParams() {
    return Get.find<AppController>().user != null
        ? {
            "ts": DateTime.now().millisecondsSinceEpoch
          }
        : {};
  }

  Future<dynamic> getWithCustomUrl(String customUrl, String path,
      {Map<String, dynamic>? params}) async {
    if (params != null) {
      params.addAll(addParams());
    } else {
      params = addParams();
    }
    final response = await RestClient.getDio(customUrl: customUrl)
        .get(path, queryParameters: params);
    return response.data;
  }

  Future<DataResponse> get(String path, {Map<String, dynamic>? params}) async {
    if (params != null) {
      params.addAll(addParams());
    } else {
      params = addParams();
    }
    final response =
        await RestClient.getDio().get(path, queryParameters: params);
    return await _handleResponse(response);
  }

  Future<DataResponse> post(String path,
      {data, bool enableCache = false}) async {
    final response = await RestClient.getDio()
        .post(path, data: data, queryParameters: addParams());
    return await _handleResponse(response);
  }

  Future<DataResponse> put(String path, {data}) async {
    final response = await RestClient.getDio()
        .put(path, data: data, queryParameters: addParams());
    return await _handleResponse(response);
  }

  Future<DataResponse> delete(String path, {data}) async {
    final response = await RestClient.getDio()
        .delete(path, data: data, queryParameters: addParams());
    return await _handleResponse(response);
  }

  Future<DataResponse> postUpload(String path, {data}) async {
    final response = await RestClient.getDio(isUpload: true)
        .post(path, data: data, queryParameters: addParams());
    return await _handleResponse(response);
  }

  Future<DataResponse> _handleResponse(dio.Response response) async {
    switch (response.statusCode) {
      case 200:
        var apiResponse = ApiResponse.fromJson(response.data);
        return DataResponse(apiResponse.data, message: apiResponse.message!);
      default:
        var apiResponse = ApiResponse.fromJson(response.data);
        throw ApiException(
          statusCode: apiResponse.status,
          message: apiResponse.message,
        );
    }
  }
}
