import 'package:flutter_chat_sdk/data/api/api_constants.dart';
import 'package:flutter_chat_sdk/data/api/models/requests/login_request.dart';
import 'package:flutter_chat_sdk/data/api/models/responses/user.dart';
import 'package:flutter_chat_sdk/data/api/services/base_service.dart';

class UserService extends BaseService {
  Future<User> loginUser(LoginRequest request) async {
    final res = await post(USER_SIGNIN, data: request.toJson());
    return User.fromJson(res.data);
  }
}
