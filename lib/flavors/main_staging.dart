import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_sdk/app/app_controller.dart';
import 'package:flutter_chat_sdk/app/chat_app.dart';
import 'package:get/get.dart';

import '../app/base_app_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  await Get.put<AppController>(AppController()).init(Environment.staging);
  runApp(const ChatApp());
}
